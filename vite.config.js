import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

process.env.VITE_ENV = process.env.NODE_ENV

process.env.VITE_PATH_PREFIX = process.env.NODE_ENV === 'production'
    ? '/' + process.env.CI_PROJECT_NAME + '/'
    : '/-/'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  // base: "/daily-todo/",
  base: process.env.VITE_PATH_PREFIX,
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  }
})

