import { MONTHS } from "./constants";

export function monthNameGenitiveCase(monthNumber) {
  return MONTHS[monthNumber];
}

export function localizedMonthName(monthNumber) {
  return new Date(`2000-${monthNumber}-1`).toLocaleDateString("default", {
    month: "long"
  });
}

export function capitalize(str) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}

export function isLeapYear(year) {
  return (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
}

export function getDaysOfMonth(month, year) {
  const date = new Date(year, month, 0)
  return date.getDate()
}

export function lastDayOfMonth(month, year) {
  const day = new Date(year, month + 1, 0)
  return day.getDay()
}