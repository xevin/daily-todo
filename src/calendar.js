import { MONTHS } from "@/constants"

export class Calendar {
  date
  static DECEMBER = 12
  static JANUARY = 1

  constructor(year=null, month=null, day=null) {
    day = day ?? 1
    month = month ?? 0

    if (year === null) {
      this.date = new Date()
    } else {
      this.date = new Date(year, month - 1, day)
    }
  }

  get day() {
    return this.date.getDate()
  }

  get month() {
    return this.date.getMonth() + 1
  }

  get year() {
    return this.date.getFullYear()
  }

  get monthNameGenitiveCase() {
    return MONTHS[this.month - 1];
  }

  get localizedMonthName() {
    return this.date.toLocaleDateString("default", {
      month: "long"
    });
  }

  get weekdayName() {
    return this.date.toLocaleDateString("default", {
      weekday: "long",
    })
  }

  get weekday() {
    return this.date.getDay()
  }

  get monthDayCount() {
    return new Date(this.year, this.month, 0).getDate()
  }

  get firstDayOfMonth() {
    return new Calendar(this.year, this.month, 1)
  }

  get lastDayOfMonth() {
    return new Calendar(this.year, this.month + 1, 0)
  }
}

Calendar.prototype.toString = function() {
  return `${this.year}-${this.month}-${this.day}`
}