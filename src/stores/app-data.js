import { defineStore } from 'pinia'

export const useAppDataStore = defineStore({
  id: 'AppData',
  state: () => ({
    // backLink = route Object {name: '...', params: {}, ...}
    backLink: null,
  }),
  getters: {
  },
  actions: {
  }
})
