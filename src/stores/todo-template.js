import { defineStore } from 'pinia'
import { customAlphabet } from "nanoid"
import { useStorage } from '@vueuse/core'

const nanoid = customAlphabet("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz", 10)

export const useTodoTemplateStore = defineStore({
  id: 'todo-template',
  state: () => ({
    templates: useStorage('templates', {
      // nanoid 0-9A-Za-z{10}
      // "BBBBB12345": {
      //   title: "Заправить постель",
      //   start: "2022-8-28",
      //   description: ""
      // },

    }),
    dailyTodo: useStorage('dailyTodo', {
      // "2022-8-28": ["BBBBB12345"],
    })
  }),
  getters: {

  },
  actions: {
    addTemplate(template) {
      const id = template.id ?? nanoid()
      this.templates[id] = template
    },
    deleteTemplate(templateId) {
      delete this.templates[templateId]
    }
  }
})
