import { createRouter, createWebHistory } from "vue-router";
import DayView from "@/views/DayView.vue";
import MonthView from "@/views/MonthView.vue";
import NotFoundView from "@/views/NotFoundView.vue";
import TodoListView from "@/views/TodoListView.vue"
import TodoEditView from "@/views/TodoEditView.vue"
import TodoCreateView from "@/views/TodoCreateView.vue"
import YearView from "@/views/YearView.vue"
import SettingsView from "@/views/SettingsView.vue"

export default createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: import.meta.env.VITE_PATH_PREFIX,
      name: "Root",
      redirect() {
        return {
          name: "Today"
        }
      },
      children: [
        {
          path: ":pathMatch(.*)*",
          name: "NotFound",
          component: NotFoundView
        },
        {
          path: "day",
          name: "Today",
          component: DayView,
        },
        {
          path: "day/:year(\\d+)-:month(\\d+)-:day(\\d+)/", // формат 2022-09-29
          component: DayView,
          name: "Day",
          props: true
        },
        {
          path: "month/:year(\\d+)-:month(\\d+)/", // формат 2022-07
          name: "Month",
          component: MonthView
        },
        {
          path: "month",
          name: "CurrentMonth",
          component: MonthView
        },
        {
          path: "year/:year(\\d+)",
          name: "Year",
          component: YearView,
          props: true
        },
        {
          path: "settings",
          name: "Settings",
          component: SettingsView
        },
        {
          path: "settings/todo-list",
          name: "TodoList",
          component: TodoListView
        },
        {
          path: "settings/todo-edit/:id",
          component: TodoEditView,
          name: "TodoEdit",
          props: true
        },
        {
          path: "settings/todo-create",
          name: "TodoCreate",
          component: TodoCreateView
        }
      ]
    },
  ]
});
